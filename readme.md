# Hopping RH front-end demo:

This is to demonstrate a possible DSP pipeline for use with the RTL-SDR and a frequency hopping front-end.

The rtlsdr_spgram_threads.c file is the culmination of the other demos. It collects samples from the RTLSDR, processes them with Liquid-DSP, and makes use of multiple cores to do so.

# software dependencies
+ automake
+ Liquid-dsp
+ librtlsdr-dev
+ FFTW (optional but recommended)

## Install (this was done on a Ubuntu 16.04 LTS Desktop VM):
sudo apt-get update  
sudo apt-get upgrade  
sudo apt-get install automake cmake libusb-1.0-0-dev  
(optional) sudo apt-get install libfftw3-dev  

git clone https://github.com/keenerd/rtl-sdr.git  
mkdir ./build  
cd ./build  
cmake ../  
make -j4  
sudo make install  
sudo ldconfig  
sudo cp ../rtl-sdr.rules /etc/udev/rules.d/rtl-sdr.rules  
sudo sh -c 'echo "blacklist dvb_usb_rtl28xxu" >> /etc/modprobe.d/blacklist.conf'  

git clone https://github.com/jgaeddert/liquid-dsp.git  
./bootstrap.sh  
CFLAGS="-march=native -O3" ./configure --enable-fftoverride  
make -j4  
sudo make install  
sudo ldconfig  

# Notes:
I didn't actually test with FFTW installed. It might be better or worse.  
I did get good multi-threading behavior, a little better than 3x improvement with 4 cores.  
I collect all of the data, then process all of the data. It would be better to process while collecting.  
I ran this on a ubuntu LTS 16.04 VM with VirtualBox on my desktop computer.  
I didn't try running on my RPi3.  