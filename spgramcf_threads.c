// Same as the spgramcf demo but this time I've incorporated some threading
// to prove that I could do it and get a benefit. I haven't used this 
// demo for a while so it might be a little outdated.
//
// gcc ./spgramcf_threads.c -o ./tspgramcf.exe -lliquid -lm -lpthread -O2

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <sys/time.h>

#include <liquid/liquid.h>

#define OUTPUT_FILENAME "spgramcf_example.py"

int n_threads;
unsigned int nfft;
float complex *buffer;
float *out_buffer;

int sample_rate = 1e6;
float guard_time = 50e-6;
float hop_time = 300e-6;
int num_hops = 1000;

void *get_result(void *param)  // param is a dummy pointer
{
    int guard_samples = sample_rate * guard_time;
    int hop_samples = sample_rate * hop_time;
    int thread_num = *(int *) param;
    // printf("Thread %d\n", thread_num);

    for (int i = thread_num; i < num_hops; i += n_threads)
    {
        spgramcf q = spgramcf_create_default(nfft);
        for (int j = 0; j < hop_samples - guard_samples; ++j)
        {
            spgramcf_push(q, buffer[i*hop_samples + j]);
        }

        spgramcf_get_psd(q, out_buffer + (i * nfft));
        spgramcf_destroy(q);
    }
}

int main() {
    // num_samples = int(sample_rate * hop_time * num_hops) + 1000
    // unsigned int num_samples =    2e6;  // number of samples
    unsigned int num_samples =    (int) sample_rate * hop_time * num_hops + 1000;  // number of samples
    float        noise_floor = -60.0f;  // noise floor [dB]
    
    // spectral periodogram options
    n_threads = 4;
    nfft = 128;  // spectral periodogram FFT size
    buffer = malloc(num_samples * sizeof(float complex));
    out_buffer = malloc(num_hops * nfft * sizeof(float));

    // derived values
    float nstd = powf(10.0f, noise_floor/20.0f);

    // generate signal (band-pass filter with Hilbert transform)
    iirfilt_rrrf filter = iirfilt_rrrf_create_prototype(
            LIQUID_IIRDES_BUTTER,
            LIQUID_IIRDES_BANDPASS,
            LIQUID_IIRDES_SOS,
            9, 0.17f, 0.20f, 0.1f, 60.0f);
    firhilbf ht = firhilbf_create(13,60.0f);

    // create samples
    for (int i=0; i<num_samples; i++) {
        // filter input noise signal
        float v = 0;
        iirfilt_rrrf_execute(filter, randnf(), &v);

        // filter off negative image (gain of 2)
        float complex y = 0;
        firhilbf_r2c_execute(ht, v, &y);

        // scale and add noise
        buffer[i] = 0.5f*y + nstd * ( randnf() + _Complex_I*randnf() ) * M_SQRT1_2;
    }

    int *val = malloc(n_threads* sizeof(int));
    pthread_t *tid = malloc( n_threads * sizeof(pthread_t) );

    struct timeval  tv1, tv2;
    gettimeofday(&tv1, NULL);

    for(int i=0; i<n_threads; i++ ){
        printf("Create thread %d.\n", i);
        val[i] = i;
        pthread_create( &tid[i], NULL, get_result, (void *) &val[i] );
    }

    for(int i=0; i<n_threads; i++ ){
        printf("Join thread %d.\n", i);

        pthread_join( tid[i], NULL );
    }
    gettimeofday(&tv2, NULL);

    printf ("Total time = %f seconds\n",
         (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
         (double) (tv2.tv_sec - tv1.tv_sec));

    // compute power spectral density output
    // float psd[nfft];
    // spgramcf_get_psd(q, psd);

    // destroy objects
    iirfilt_rrrf_destroy(filter);
    firhilbf_destroy(ht);
    // spgramcf_destroy(q);

    //
    // export output file
    //
    // FILE * fid = fopen(OUTPUT_FILENAME,"w");
    // fprintf(fid,"#%% %s : auto-generated file\n", OUTPUT_FILENAME);
    // fprintf(fid,"import numpy as np\n");
    // fprintf(fid,"import matplotlib.pyplot as plt\n");
    // fprintf(fid,"nfft = %u;\n", num_hops * nfft);
    // fprintf(fid,"psd  = np.zeros(nfft)\n");
    // fprintf(fid,"noise_floor = %12.6f\n", noise_floor);

    // for (i=0; i<num_hops * nfft; i++)
    //     fprintf(fid,"psd[%6u] = %12.4e\n", i, out_buffer[i]);

    // fprintf(fid,"plt.plot(psd, '-')\n");
    // fprintf(fid,"plt.show()\n");

    // fclose(fid);
    // printf("results written to %s.\n", OUTPUT_FILENAME);

    // printf("done.\n");
    return 0;
}

