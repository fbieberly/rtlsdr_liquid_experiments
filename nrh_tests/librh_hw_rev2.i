/**
 * librh_hw_Rev2.i
 * 
 * Internal API functions only.
 * 
 * Author(s): Abbas
 * 
 */

#ifndef RHRTLSDRLIB_I
#define RHRTLSDRLIB_I

/*
 * This head is used for intneral API functions only.
 */

/** Initialize the PLL of front end. **/
int pllInit(void);

/** Convenience Function for pllTune **/
int vcoSubsys2(int k);

/** Set the frequency of PLL **/
int pllTune(float freq);

/** Select the output through MUX **/
int selectOut(float freq);

/** Communication of settings to MSP430 **/
int msp_spi_command(uint16_t fcstart, uint16_t fcstop, uint16_t step_size, uint16_t nsamples, uint16_t fsamp, uint8_t vga_gain, int vga_setting);

/** MSP start scan command **/
int msp_start_scan(void);

/** HW detect utility **/
int hw_detect(void);

/** MSP reset utility **/
int msp_reset(void);

#endif /* RHRTLSDRLIB_I */
