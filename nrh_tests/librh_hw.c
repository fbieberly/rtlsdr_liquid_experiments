/**
 * librh_hw.c
 *
 * This library wraps the rtl-sdr and bcm2835 libraries into a single 
 * radiohound hardware library with added functionality.
 * 
 * The library contains functions for controlling the radiohound 
 * hardware, including dongle operations and SPI control of the PLL.
 * 
 * Author(s): Abbas, Eric
 * 
 */

#include "librh_hw.h"
#include "librh_hw.i"
#include <stdio.h>
#include <math.h>

#define	HW_PIN 	RPI_V2_GPIO_P1_31
#define	A0 	RPI_GPIO_P1_22
#define	A1 	RPI_GPIO_P1_18
#define	PLL_EN	RPI_V2_GPIO_P1_13
#define X5_S1	RPI_GPIO_P1_15
#define LNA_EN 	RPI_GPIO_P1_16

static int hwdet = 0;
static float MAX_VGA_GAIN = 45.0;

#ifdef __linux__

#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <bcm2835.h>

#define BCM2835_INIT() bcm2835_init()

/** Start SPI communications **/
int spiOpen(void){
	int fd = BCM2835_INIT();
    if (!fd) return -1;
	else{
		return fd;
	}
}


/** End SPI communicaions **/
int spiClose(void){
	return 0;
}


/** Initialize the PLL of front end. **/
int pllInit(void){
	//BCM2835_INIT();
	bcm2835_spi_begin();
	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
	bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_65536);
	bcm2835_spi_chipSelect(BCM2835_SPI_CS0);
	bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, HIGH);
	//The format is
	//1 W/R bit
	//6 bit reg address
	//24 data bits
	//1 extra bit
	char tbuf0[]={0b00001110,0b00000000,0b00000010,0b10011010};	//&H7 &H14D
	bcm2835_spi_transfern(tbuf0,4);
	char tbuf[]={0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
	bcm2835_spi_transfern(tbuf,4);
	char tbuf2[]={0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
	bcm2835_spi_transfern(tbuf2,4);
	char tbuf3[]={0b00000000,0b00010100,0b11110010,0b11101010};	//&H0 &HA7975
	bcm2835_spi_transfern(tbuf3,4);
	char tbuf4[]={0b00000010,0b00000000,0b00000000,0b00000100};	//&H1 &H2
	bcm2835_spi_transfern(tbuf4,4);
	//char tbuf5[]={0b00000100,0b00000000,0b00000000,0b00000010};	//&H2 &H1
	char tbuf5[]={0b00000100,0b00000000,0b00000000,0b00001010};	//&H2 &H5 reference Divider = 5
	bcm2835_spi_transfern(tbuf5,4);
	char tbuf23[]={0b00000110,0b00000000,0b00000000,0b01111000};	//&H3 &H3C
	bcm2835_spi_transfern(tbuf23,4);
	char tbuf6[]={0b00001010,0b00000000,0b00101100,0b01010000};	//&H5 &H1628
	bcm2835_spi_transfern(tbuf6,4);
	char tbuf7[]={0b00001010,0b00000000,0b11000001,0b01000000};	//&H5 &H60A0
	bcm2835_spi_transfern(tbuf7,4);
	char tbuf8[]={0b00001010,0b00000000,0b00000000,0b00000000};	//&H5 &H0
	bcm2835_spi_transfern(tbuf8,4);
	char tbuf9[]={0b00001100,0b00000110,0b00011110,0b10010100};	//&H6 &H30F4A
	bcm2835_spi_transfern(tbuf9,4);
	char tbuf10[]={0b00001110,0b00000000,0b00000010,0b10011010};	//&H7 &H14D
	bcm2835_spi_transfern(tbuf10,4);
	char tbuf11[]={0b00010001,0b10000011,0b01111101,0b11111110};	//&H8 &HC1BEFF
	bcm2835_spi_transfern(tbuf11,4);
	char tbuf12[]={0b00010010,0b10101000,0b11111111,0b11111110};	//&H9 &H547FFF
	bcm2835_spi_transfern(tbuf12,4);
	char tbuf13[]={0b00010100,0b00000000,0b01000000,0b10001100};	//&HA &H2046
	bcm2835_spi_transfern(tbuf13,4);
	char tbuf14[]={0b00010110,0b00001111,0b10000000,0b01000010};	//&HB &H7C021
	bcm2835_spi_transfern(tbuf14,4);
	char tbuf15[]={0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
	bcm2835_spi_transfern(tbuf15,4);
	char tbuf16[]={0b00011010,0b00000000,0b00000000,0b00000000};	//&HD &H0
	bcm2835_spi_transfern(tbuf16,4);
	char tbuf17[]={0b00011100,0b00000000,0b00000000,0b00000000};	//&HE &H0
	bcm2835_spi_transfern(tbuf17,4);
	char tbuf18[]={0b00011110,0b00000000,0b00000000,0b00000010};	//&HF &H1
	bcm2835_spi_transfern(tbuf18,4);
	char tbuf19[]={0b00100000,0b00000000,0b00000001,0b01100100};	//&H10 &HB2
	bcm2835_spi_transfern(tbuf19,4);
	char tbuf20[]={0b00100010,0b00010000,0b00000000,0b00000100};	//&H11 &H80002
	bcm2835_spi_transfern(tbuf20,4);
	char tbuf21[]={0b00100100,0b00000000,0b00000000,0b00000110};	//&H12 &H3
	bcm2835_spi_transfern(tbuf21,4);
	char tbuf22[]={0b00100110,0b00000000,0b00100100,0b10110010};	//&H13 &H1259
	bcm2835_spi_transfern(tbuf22,4);
	char tbuf24[]={0b0001000,0b00110011,0b00110011,0b00110100};	//&H4 &H19999A
	bcm2835_spi_transfern(tbuf24,4);
	char tbuf25[]={0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
	bcm2835_spi_transfern(tbuf25,4);	
	char tbuf26[]={0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
	bcm2835_spi_transfern(tbuf26,4);
	char tbuf27[]={0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
	bcm2835_spi_transfern(tbuf27,4);
	
	
	bcm2835_spi_end();
	return 0;
}

/** Convenience function for pllTune **/
int vcoSubsys2(int k){
		// 0b [0 11 kkkkkk] 0010 000
		return (0x6010 | (k << 7));

		// 0b [0 00 kkkkkk] 0010 000
		//return (0x0010 | (k << 7));

}

/** Set the frequency of PLL **/
int pllTune(float freq){
	bcm2835_spi_begin();
	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
	bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_65536);
	bcm2835_spi_chipSelect(BCM2835_SPI_CS0);
	bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, HIGH);
	
		
	int k;
	float k1;
	int f_osc = 50; 	//MHz
	int R_div = 1;
	int f_pd = f_osc/R_div;

	int R_div_0 = (R_div << 1) & 0b11111111;
	int R_div_1 = (R_div >> 7) & 0b11111111;
	int R_div_2 = (R_div >> 15) & 0b11111111;
	int R_div_3 = (R_div >> 23) & 0b00000001;
	R_div_3 = R_div_3 | 0b00000100;

	int Nint,Nfrac;
		
	if(freq>3000){
		k1=0.5;
		Nint = floor(k1*freq/f_pd);
		Nfrac = round((k1*freq/f_pd-Nint)*16777216);
		//printf("k is %f \n" ,k1 );
	}
	else{
		k = floor(3000/freq);
		if(k>1){
			k = k - (k%2);
		}
		Nint = floor(k*freq/f_pd);
		Nfrac = round((k*freq/f_pd-Nint)*16777216);
		//printf("k is %d \n" ,k );
	}
	
	float pll_freq;
								
	//printf("int is %d \n" ,Nint );
	//printf("frac is %d \n" ,Nfrac );
	int Nfrac_0 = (Nfrac << 1) & 0b11111111;
	int Nfrac_1 = (Nfrac >> 7) & 0b11111111;
	int Nfrac_2 = (Nfrac >> 15) & 0b11111111;
	int Nfrac_3 = (Nfrac >> 23) & 0b00000001;
	Nfrac_3 = Nfrac_3 | 0b00001000;
	
	int Nint_0 = (Nint << 1) & 0b11111111;
	int Nint_1 = (Nint >> 7) & 0b11111111;
	int Nint_2 = (Nint >> 15) & 0b00001111;
	
	if(freq > 3000){
		pll_freq = (f_pd)*(Nint+Nfrac/16777216.00)/k1;
		printf("Tuning PLL to %0.10g MHz\n" , pll_freq);

		char tbuf[] = {0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
		bcm2835_spi_transfern(tbuf,4);
		char tbuf2[] = {0b00001010,0b00000000,0b11000001,0b00100000};	//&H5 &H6090 set divider accordingly
//		char tbuf2[] = {0b00001010,0b00000000,0b00000001,0b00100000};	//&H5 &H6090 set divider accordingly
		bcm2835_spi_transfern(tbuf2,4);
		char tbuf3[] = {0b00001010,0b00000001,0b01011010,0b00000000};	//&H5 &HAD00 Using High freq sub band
		bcm2835_spi_transfern(tbuf3,4);
		char tbuf4[] = {0b00010010,0b10101000,0b11111111,0b11111110};    //&H9 &H547FFF //Charge Pump
		bcm2835_spi_transfern(tbuf4,4);
		char tbuf5[] = {0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
		bcm2835_spi_transfern(tbuf5,4);
		char tbuf6[] = {0b00001100,0b00000110,0b00011110,0b10010100};	//&H6 &H30F4A //Sigma Delta Bypass for Integer mode
		bcm2835_spi_transfern(tbuf6,4);
		char tbuf7[] = {0b00001100,0b00000110,0b00011110,0b10010100};	//&H6 &H30F4A
		bcm2835_spi_transfern(tbuf7,4);
//		char tbuf8[] = {0b00000100,0b00000000,0b00000000,0b00000010};	//&H2 &H1 // set reference divider to 1
		char tbuf8[] = {(char)R_div_3,(char)R_div_2,(char)R_div_1,(char)R_div_0};	//&H2 &H1 // set reference divider to 5
		bcm2835_spi_transfern(tbuf8,4);
		char tbuf9[] = {0b00001010,0b00000000,0b01000000,0b00110000};	//&H5 &H2018 Set to doubler mode
		bcm2835_spi_transfern(tbuf9,4);
		char tbuf10[] = {0b00001010,0b00000000,0b00000000,0b00000000};	//&H5 &H0
		bcm2835_spi_transfern(tbuf10,4);
		char tbuf11[] = {0b00000110,(char)Nint_2,(char)Nint_1,(char)Nint_0}; // &H3 Set integer reg to freq
		bcm2835_spi_transfern(tbuf11,4);
		char tbuf12[] = {0b00001010,0b00000000,0b00000000,0b00000000};    //&H5 &H0
		bcm2835_spi_transfern(tbuf12,4);
		char tbuf13[] = {(char)Nfrac_3,(char)Nfrac_2,(char)Nfrac_1,(char)Nfrac_0};	//&H4 Set Frequency Fractional Part
		bcm2835_spi_transfern(tbuf13,4);
		char tbuf14[] = {0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0					
		bcm2835_spi_transfern(tbuf14,4);					

	} else {
		
		pll_freq = f_pd*(Nint+Nfrac/16777216.00)/k;
		printf("Tuning PLL to %0.10g MHz\n" , pll_freq);
		int vcoSub = vcoSubsys2(k);
		int vcoSub_0 = (vcoSub << 1) & 0b11111111;
		int vcoSub_1 = (vcoSub >> 7) & 0b11111111;
		
		char tbuf[]={0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
		bcm2835_spi_transfern(tbuf,4);					
		char tbuf2[]={0b00001010,0b00000000,(char)vcoSub_1,(char)vcoSub_0};	//&H5 &H6090 set divider accordingly
		bcm2835_spi_transfern(tbuf2,4);					
		char tbuf3[]={0b00001010,0b00000000,0b01000010,0b00000000};	//&H5 &H2100 tuning default
		bcm2835_spi_transfern(tbuf3,4);						
		char tbuf4[] = {0b00010010,0b10101000,0b11111111,0b11111110};    //&H9 &H547FFF //Charge Pump
		bcm2835_spi_transfern(tbuf4,4);
		char tbuf5[]={0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
		bcm2835_spi_transfern(tbuf5,4);
		char tbuf6[]={0b00001100,0b00000110,0b00011110,0b10010100};	//&H6 &H30F4A //Sigma Delta Bypass for Integer mode
		bcm2835_spi_transfern(tbuf6,4);
		char tbuf7[]={0b00001100,0b00000110,0b00011110,0b10010100};	//&H6 &H30F4A
		bcm2835_spi_transfern(tbuf7,4);
		//char tbuf8[]={0b00000100,0b00000000,0b00000000,0b00000010};	//&H2 &H1 // set reference divider to 1
		char tbuf8[] = {(char)R_div_3,(char)R_div_2,(char)R_div_1,(char)R_div_0};	//&H2 &H1 // set reference divider to R_div
		bcm2835_spi_transfern(tbuf8,4);					
		char tbuf9[]={0b00001010,0b00000000,0b01010001,0b00110000};	//&H5 &H2898 Set to funcdamental Mode
		bcm2835_spi_transfern(tbuf9,4);					
		char tbuf10[]={0b00001010,0b00000000,0b00000000,0b00000000};	//&H5 &H0
		bcm2835_spi_transfern(tbuf10,4);
		char tbuf11[]={0b00000110,(char)Nint_2,(char)Nint_1,(char)Nint_0};//& H3Set integer reg to freq
		bcm2835_spi_transfern(tbuf11,4);	
		char tbuf12[]={0b00001010,0b00000000,0b00000000,0b00000000};	//&H5 &H0
		bcm2835_spi_transfern(tbuf12,4);
		char tbuf13[]={(char)Nfrac_3,(char)Nfrac_2,(char)Nfrac_1,(char)Nfrac_0};//&H4 Set Frequency Fractional Part
		bcm2835_spi_transfern(tbuf13,4);	
		char tbuf14[]={0b00011000,0b00000000,0b00000000,0b00000000};	//&HC &H0
		bcm2835_spi_transfern(tbuf14,4);	
	}
	
    	bcm2835_spi_end();
    
	return pll_freq;
}

int hw_detect(void){
	//set HW_PIN to be an input
	bcm2835_gpio_fsel(HW_PIN, BCM2835_GPIO_FSEL_INPT);
	// with a pulldown
	bcm2835_gpio_set_pud(HW_PIN, BCM2835_GPIO_PUD_DOWN);
	//read HW_PIN value
	return bcm2835_gpio_lev(HW_PIN);
}

/** Select the output through MUX **/
int selectOut(float freq){
	
	bcm2835_gpio_fsel(A0, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(A1, BCM2835_GPIO_FSEL_OUTP);

	if (freq>=400e6) {
	bcm2835_gpio_write(A0,LOW);
	bcm2835_gpio_write(A1,HIGH);
	}
	else if (freq<100e6) {
	bcm2835_gpio_write(A0,LOW);
	bcm2835_gpio_write(A1,LOW);
	}
	else {
	bcm2835_gpio_write(A0,HIGH);
	bcm2835_gpio_write(A1,LOW);
	}
	
	//bcm2835_close();
	return 1;
}



/** Set the front end VGA Gain **/
int rh_set_vga_gain(float gain){
	
	float DEFAULT_VGA_GAIN = 13.5;
	if((gain < 13.5) || (gain > 45.0) || (fmod(gain, 0.5) != 0.0))				/*To verify if gain is in the range of 13.5dB >= gain >= 45dB, and in steps of 0.5dB.*/
	{
        	printf("Error! Enter a gain between 13.5 and 45.0, in steps of 0.5.\n""Defaulting to 13.5 dB. \n");
		rh_set_vga_gain(DEFAULT_VGA_GAIN);
	}

	else
	{
		/*Variable declarations*/
		float initial_gain;
		float step;
		char serial_word;
		char data_transmit;
		int status;
		
	        /*SPI communication setup*/
		status = BCM2835_INIT();
	        if(!status) return(1); 								/*Change to return a message instead of returning 1.*/
	        bcm2835_spi_begin(); 								/*Start SPI0 communications, open SPI0(SPI port 0).*/
	        bcm2835_spi_chipSelect(BCM2835_SPI_CS1); 					/*Chip select, device 0(CE0 pin)*/
	        bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS1, LOW); 			/*LE pin on VGA is active low.*/
	        bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST); 			/*VGA is MSB Fisrt.*/
	        bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_32); 			/*32 = 128nS = 7.8125MHz*/
	
	        /*Data transfer from Raspberry Pi to VGA via SPI communication*/
	        initial_gain = 13.5; 								/*HMC681ALP5E VGA start with 13.5dB. It can be programmed to provide anywhere from 13.5, to 45dB of gain.*/
	        step = 0.5; 									/*The gain control can be incresed/decreased in 0.5dB steps.*/
	        serial_word = (gain - initial_gain)/step; 					/*This equation calculate the serial word to be transmitted to the VGA.*/
	        data_transmit = bcm2835_spi_transfer(serial_word); 				/*Transfer one byte: serial_word. The function returns the read data byte from the slave.*/
	        //printf("%d\n",data_transmit);
        	bcm2835_spi_end(); 								/*End SPI operations. Closes the port before exit.*/
	}
return 1;
}

#else
/*
 * not Linux: use dummy functions to allow the RTL passthrough
 * functions to build and work
 */

#define BCM2835_INIT()

/** Start SPI communications **/
int spiOpen(void) { return -1; }

/** End SPI communicaions **/
int spiClose(void) { return -1; }

/** Initialize the PLL of front end. **/
int pllInit(void) { return -1; }

/** Convenience Function for pllTune **/
int vcoSubsys2(int k) { return -1; }

/** Set the frequency of PLL **/
int pllTune(float freq) { return -1; }

/** Select the output through MUX **/
int selectOut(float freq) { return -1; }

/** is RH HW detected **/
int hw_detect(void) { return 0; }

#endif /* __linux */

/** Open SDR device **/
int rh_open(rh_dev_t **out_dev, uint32_t index){
	BCM2835_INIT();
        printf("HW DET: %d\n.", hw_detect());
	int r = rtlsdr_open(out_dev,index);
	if (r>=0){
		char vendor[256], product[256], serial[256];
		rtlsdr_get_device_usb_strings(index, vendor, product, serial);
		printf("Using device %d:\n\tName: %s\n\tVendor: %s\n\tProduct: %s\n\tSN: %s\n\n", index, rtlsdr_get_device_name(index), vendor, product, serial);
		hwdet = hw_detect();
		if (!hwdet)
			printf("RF extension hardware not detected! Maximum Tuning frequency is 1.7 GHz...\n");
		else{
			printf("RF extension hardware detected...\n");
			bcm2835_gpio_fsel(PLL_EN, BCM2835_GPIO_FSEL_OUTP);
			bcm2835_gpio_write(PLL_EN,HIGH);
			bcm2835_gpio_fsel(X5_S1, BCM2835_GPIO_FSEL_OUTP);
			bcm2835_gpio_write(X5_S1, LOW);
			bcm2835_gpio_fsel(LNA_EN, BCM2835_GPIO_FSEL_OUTP);
			bcm2835_gpio_write(LNA_EN, LOW);
		}
	}
	return r;
}


/** Close SDR device **/
int rh_close(rh_dev_t *dev){
	return rtlsdr_close(dev);
}


/** Set the frequency of the dongle **/
static uint64_t freq_64;
int rh_set_center_freq(rh_dev_t *dev, uint64_t freq){
        freq_64 = freq;
	selectOut(freq);
	if (freq<400e6 || !hwdet){
		uint32_t freq_32 = (uint32_t) freq;
		return rtlsdr_set_center_freq(dev,freq_32);
	}
	else {
		//Set rtlsdr tuner to 110 MHz, and tune dongle to fc-110 MHz
		uint32_t freq_t = 110000000;
		int result = rtlsdr_set_center_freq(dev, freq_t);
		freq = (freq-freq_t)*1e-6;
		pllInit();
		pllTune(freq);
		return result;
	}
}


/** Get the frequency of the dongle **/
uint64_t rh_get_center_freq(rh_dev_t *dev){
         return freq_64;
}

/** Set the sample rate **/
int rh_set_sample_rate(rh_dev_t *dev, uint32_t samp_rate){
	return rtlsdr_set_sample_rate(dev,samp_rate);
}


/** Get the sample rate **/
uint32_t rh_get_sample_rate(rh_dev_t *dev){
	return rtlsdr_get_sample_rate(dev);
}

/** Set the gain manually **/
int rh_set_gain(rh_dev_t *dev, int gain){

	if(!hwdet){

		rtlsdr_set_tuner_gain_mode(dev, 1);
		return rtlsdr_set_tuner_gain(dev, (int) (gain*10));
	}
	else if (gain <MAX_VGA_GAIN){
		rh_set_vga_gain(gain);
		rtlsdr_set_tuner_gain_mode(dev, 1);
		return rtlsdr_set_tuner_gain(dev, 50); 		//consistently shift RTLSDR tuner gain by 5 dB
	}
	else{
		rh_set_vga_gain(MAX_VGA_GAIN);
		rtlsdr_set_tuner_gain_mode(dev, 1);
		return rtlsdr_set_tuner_gain(dev,(int) ((gain-MAX_VGA_GAIN)*10 +50));
	}
}

/** Get the gain **/
int rh_get_gain(rh_dev_t *dev){
	return rtlsdr_get_tuner_gain(dev);
}


/** Set the ppm **/
int rh_set_freq_correction(rh_dev_t *dev, int ppm_error){
	return rtlsdr_set_freq_correction(dev, ppm_error);
}


/** Get the ppm **/
int rh_get_freq_correction(rh_dev_t *dev){
	return rtlsdr_get_freq_correction(dev);
}


/** Reset buffer **/
int rh_reset_buffer(rh_dev_t *dev){
	return rtlsdr_reset_buffer(dev);
}


/** Read samples synchronously **/
int rh_read_sync(rh_dev_t *dev, void *buf, int len, int *n_read){
	return rtlsdr_read_sync(dev, buf, len, n_read);
}
