/**
 * librh_hw.h
 * 
 * This library wraps the rtl-sdr and bcm2835 libraries into a single 
 * radiohound hardware library with added functionality.
 * 
 * The library contains functions for controlling the radiohound 
 * hardware, including dongle operations and SPI control of the PLL.
 * 
 * Author(s): Abbas, Eric
 * 
 */

#ifndef RHRTLSDRLIB_H
#define RHRTLSDRLIB_H

#include <rtl-sdr.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef rtlsdr_dev_t rh_dev_t;
typedef rtlsdr_read_async_cb_t rh_read_async_cb_t;

/** Open SDR device **/
int rh_open(rh_dev_t **out_dev, uint32_t index);

/** Close SDR device **/
int rh_close(rh_dev_t *dev);

/** Set the frequency of the radiohound device **/
//int rh_set_center_freq(rh_dev_t *dev, uint32_t freq);

/** Set the frequency of the radiohound device **/
int rh_set_center_freq(rh_dev_t *dev, uint64_t freq);

/** Get the frequency of the radiohound device **/
uint64_t rh_get_center_freq(rh_dev_t *dev);

/** Set the sample rate **/
int rh_set_sample_rate(rh_dev_t *dev, uint32_t samp_rate);

/** Get the sample rate **/
uint32_t rh_get_sample_rate(rh_dev_t *dev);

/** Set the gain manually **/
int rh_set_gain(rh_dev_t *dev, int gain);

/** Get the gain **/
int rh_get_gain(rh_dev_t *dev);

/** Set the ppm **/
int rh_set_freq_correction(rh_dev_t *dev, int ppm_error);

/** Get the ppm **/
int rh_get_freq_correction(rh_dev_t *dev);

/** Reset buffer **/
int rh_reset_buffer(rh_dev_t *dev);

/** Read samples synchronously **/
int rh_read_sync(rh_dev_t *dev, void *buf, int len, int *n_read);

/********************************/
/* The following are just pass-through functions for now.  These are
 * used internally by the client-gr rh_rtl_source block.  #define for
 * the actual functionality unless needed in some other capacity.
 */

#define rh_read_async             rtlsdr_read_async
#define rh_cancel_async           rtlsdr_cancel_async
#define rh_get_device_count       rtlsdr_get_device_count
#define rh_get_device_name        rtlsdr_get_device_name
#define rh_get_device_usb_strings rtlsdr_get_device_usb_strings
#define rh_get_index_by_serial    rtlsdr_get_index_by_serial
#define rh_get_tuner_gain         rtlsdr_get_tuner_gain
#define rh_get_tuner_gains        rtlsdr_get_tuner_gains
#define rh_get_tuner_type         rtlsdr_get_tuner_type
#define rh_get_xtal_freq          rtlsdr_get_xtal_freq
#define rh_set_agc_mode           rtlsdr_set_agc_mode
#define rh_set_direct_sampling    rtlsdr_set_direct_sampling
#define rh_set_offset_tuning      rtlsdr_set_offset_tuning
#define rh_set_tuner_gain         rtlsdr_set_tuner_gain
#define rh_set_tuner_gain_mode    rtlsdr_set_tuner_gain_mode
#define rh_set_tuner_if_gain      rtlsdr_set_tuner_if_gain
#define rh_set_xtal_freq          rtlsdr_set_xtal_freq
#define rh_tuner                  rtlsdr_tuner                      

#ifdef __cplusplus
}
#endif

#endif /* RHRTLSDRLIB_H */
