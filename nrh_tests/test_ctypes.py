import ctypes
import time

import numpy as np
from numpy.ctypeslib import ndpointer
import matplotlib.pyplot as plt
import scipy.signal as scisig

lib = ctypes.cdll.LoadLibrary("./rtlsdr_ctypes_lib.so")
c_get_samps = lib.get_samples
c_get_samps.restype = ctypes.c_int
c_get_samps.argtypes = [ctypes.c_int, 
                        ctypes.c_int, 
                        ctypes.c_int, 
                        ctypes.c_int, 
                        ndpointer(ctypes.c_uint8, flags='C_CONTIGUOUS')]

c_get_scan_samps = lib.get_scan_samples
c_get_scan_samps.restype = ctypes.c_int
c_get_scan_samps.argtypes = [ctypes.c_int, 
                          ctypes.c_int, 
                          ctypes.c_int, 
                          ctypes.c_int, 
                          ctypes.c_int, 
                          ndpointer(ctypes.c_uint8, flags='C_CONTIGUOUS')]

c_open_rtlsdr = lib.open_rtlsdr
c_open_rtlsdr.restype = ctypes.c_int
c_open_rtlsdr.argtypes = [ctypes.c_int]

c_close_rtlsdr = lib.close_rtlsdr
c_close_rtlsdr.restype = ctypes.c_int
# c_open_rtlsdr.argtypes = [ctypes.c_int]

MIN_FREQ = 50
MAX_FREQ = 1700
MIN_RBW = 1000
MAX_RBW = 100000
MIN_GAIN = 50
MAX_GAIN = 400
SAMP_RATE = 2400000
SCAN_OFFSET = 1 # start scanning 1 MHz above start_frequency

def get_samples(start_freq, stop_freq, rbw, samps_per_bin=8):
    """
    Collects samples from the RTL-SDR and returns a numpy array.
    start_freq: int, starting frequency in MHz
    stop_freq: int, stopping frequency in MHz
    rbw: int, resolution bandwidth in Hz
    samps_per_bin: a multiple of how big the fft size will be
    
    """
    start_freq = int(start_freq)
    stop_freq = int(stop_freq)
    rbw = int(rbw)
    samps_per_bin = int(samps_per_bin)
    if not (MIN_FREQ <= start_freq < stop_freq): raise ValueError("Invalid start_frequency")
    if not (start_freq < stop_freq <= MAX_FREQ): raise ValueError("Invalid stop_frequency")
    if not (MIN_RBW <= rbw <= MAX_RBW): raise ValueError("Invalid RBW")
    if not (1 <= samps_per_bin <= 10): raise ValueError("Invalid samples per bin")

    samps_per_hop = SAMP_RATE/rbw*samps_per_bin


    out_samps = np.zeros(samps_per_hop*2, dtype=np.uint8)

    ret = c_get_samps(start_freq, stop_freq, rbw, samps_per_bin, out_samps)
    if ret != 0: raise Error("Failed to get samples: Error code {}".format(ret))
    out_samps = out_samps.astype(np.float32) - 128
    out_samps = out_samps.view(np.complex64)
    return out_samps

def get_scan_samps(start_freq, stop_freq, rbw, samps_per_bin=5, gain=100):
    """
    Collects samples from the RTL-SDR and returns a numpy array from all the hops
    of a single scan.
    start_freq: int, starting frequency in MHz
    stop_freq: int, stopping frequency in MHz
    rbw: int, resolution bandwidth in Hz
    samps_per_bin is a multiple of how big the fft size will be
    gain: front end gain of the rtlsdr, is gain in dB * 10
    """
    start_freq = int(start_freq)
    stop_freq = int(stop_freq)
    rbw = int(rbw)
    samps_per_bin = int(samps_per_bin)
    gain = int(gain)
    if not (MIN_FREQ <= start_freq < stop_freq): raise ValueError("Invalid start_frequency")
    if not (start_freq < stop_freq <= MAX_FREQ): raise ValueError("Invalid stop_frequency")
    if not (MIN_RBW <= rbw <= MAX_RBW): raise ValueError("Invalid RBW")
    if not (1 <= samps_per_bin <= 10): raise ValueError("Invalid samples per bin")
    if not (MIN_GAIN <= gain <= MAX_GAIN): raise ValueError("Invalid gain")

    samps_per_hop = SAMP_RATE/rbw*samps_per_bin

    num_hops = 1
    if (stop_freq - start_freq) > 2:
        # This is a kind of gross way of figuring how many hops are needed
        num_hops = int((stop_freq - start_freq)/2.0 + 0.5)

    out_samps = np.zeros(samps_per_hop*2 * num_hops, dtype=np.uint8)

    ret = c_get_scan_samps(start_freq + SCAN_OFFSET, num_hops, rbw, samps_per_bin, gain, out_samps)
    if ret != 0: raise RuntimeError("Failed to get samples: Error code {}".format(ret))
    out_samps = out_samps.astype(np.float32) - 128
    out_samps = out_samps.view(np.complex64)
    return out_samps

def get_scan_periodgram(start_freq, stop_freq, rbw, samps_per_bin=5, gain=100):
    """
    returns two arrays, the frequencies and psd
    """
    start_freq = int(start_freq)
    stop_freq = int(stop_freq)
    rbw = int(rbw)
    samps_per_bin = int(samps_per_bin)
    gain = int(gain)
    if not (MIN_FREQ <= start_freq < stop_freq): raise ValueError("Invalid start_frequency")
    if not (start_freq < stop_freq <= MAX_FREQ): raise ValueError("Invalid stop_frequency")
    if not (MIN_RBW <= rbw <= MAX_RBW): raise ValueError("Invalid RBW")
    if not (1 <= samps_per_bin <= 10): raise ValueError("Invalid samples per bin")
    if not (MIN_GAIN <= gain <= MAX_GAIN): raise ValueError("Invalid gain")

    samps_per_hop = SAMP_RATE/rbw*samps_per_bin

    num_hops = 1
    if (stop_freq - start_freq) > 2:
        # This is a kind of gross way of figuring how many hops are needed
        num_hops = int((stop_freq - start_freq)/2.0 + 0.5)

    out_samps = np.zeros(samps_per_hop*2 * num_hops, dtype=np.uint8)

    sub_tic = time.time()
    ret = c_get_scan_samps(start_freq + SCAN_OFFSET, num_hops, rbw, samps_per_bin, gain, out_samps)
    sub_toc = time.time()
    print "DA Time: ", sub_toc - sub_tic

    sub_tic = time.time()
    if ret != 0: raise RuntimeError("Failed to get samples: Error code {}".format(ret))
    out_samps = out_samps.astype(np.float32) - 128
    out_samps = out_samps.view(np.complex64)



    out_freqs = np.arange(start_freq*1e6, stop_freq*1e6, rbw)
    out_psd = np.zeros(len(out_freqs))
    valid_bins = int(2e6/rbw)
    nfft = int(SAMP_RATE/rbw)
    side_bins = int(nfft - valid_bins)/2

    # print len(out_freqs)
    # print len(out_psd)
    # print valid_bins
    # print nfft
    # print side_bins
    # print samps_per_hop
    # print samps_per_bin
    size_psd = len(out_psd)
    
    samps_start_idx = 0
    psd_idx = 0
    welch = scisig.welch
    for xx in xrange(num_hops):
        temp_samps = out_samps[samps_start_idx:samps_start_idx+samps_per_hop]
        f, Pxx_den = welch(temp_samps, window='hanning',
                                  nperseg=nfft, scaling='spectrum')

        psd_fill_size = valid_bins
        if size_psd - psd_idx < valid_bins:
            psd_fill_size = size_psd - psd_idx
        out_psd[psd_idx:psd_idx+psd_fill_size] = Pxx_den[side_bins:side_bins+psd_fill_size]

        samps_start_idx += samps_per_hop
        psd_idx += valid_bins

    sub_toc = time.time()
    print "SigPro Time: ", sub_toc - sub_tic
    return out_psd, out_freqs

def open_rtlsdr(dev_index=0):

    dev = ctypes.c_void_p()
    r = c_open_rtlsdr(dev_index)
    if r != 0: print("Did not successfully open rtlsdr.")

    return dev

def close_rtlsdr():

    r = c_close_rtlsdr()
    if r != 0: print("Did not successfully close rtlsdr.")

# out_samps = get_samples(100, 200, 10e3)
# out_samps = get_scan_samps(80, 1080, 10e3, 5, 400)

start_freq = 90
stop_freq = 1090
rbw = 50e3
samps_per_bin = 10
gain = 300



open_rtlsdr()
tic = time.time()
out_samps, out_freqs = get_scan_periodgram(start_freq, 
                                           stop_freq, 
                                           rbw, 
                                           samps_per_bin, 
                                           gain)
close_rtlsdr()

toc = time.time()
print "Time taken: ", toc-tic

# plt.semilogy(out_freqs/1e6, out_samps.real)
# plt.show()
