// simple example of how to get samples from an RTLSDR
// I used the rtlsdr_read_sync method of getting samples
// there is an async version that you might be able to use better
// 
// gcc ./librh_hw_rev2.c ./rtl_time_read_freq.c -std=gnu99 -lbcm2835 -lrtlsdr -lm -I/home/gh/rtlsdr_liquid_experiments/nrh_tests -o ./rtl_time_read_freq.exe

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

// #include "rtl-sdr.h"
#include "librh_hw.h"

#define BUFFER_SIZE 1000000
static rtlsdr_dev_t *dev = NULL;

struct timespec start, end;

int main(int argc, char **argv)
{
    uint8_t *buffer = malloc(BUFFER_SIZE * sizeof(uint8_t));

    ///
    // init radio
    ///
    int device_count = rh_get_device_count();
    if (!device_count)
    {
        fprintf(stderr, "No supported devices found.\n");
        exit(1);
    }

    fprintf(stderr, "Found %d device(s):\n", device_count);

    uint32_t dev_index = 0;
    fprintf(stderr, "Using device %d: %s\n", dev_index, rh_get_device_name(dev_index));

    int r = rh_open(&dev, dev_index);
    if (r < 0)
    {
        fprintf(stderr, "Failed to open rtlsdr device #%d.\n", dev_index);
        exit(1);
    }
    fprintf(stderr, "Opened RTLSDR.");
    
    int num_hops = 100;
    int frequency = 100000000;
    r = rh_set_center_freq(dev, frequency);
    if (r < 0) fprintf(stderr, "WARNING: Failed to read center freq.\n");

    clock_gettime(NULL, &start);
    for (int i = 0; i < num_hops; ++i)
    {
        uint64_t freq = rh_get_center_freq(dev);

        if (freq != frequency) fprintf(stderr, "WARNING: Failed to read center freq.\n");
    }
    clock_gettime(NULL, &end);
    rh_close(dev);

    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;

    fprintf(stderr, "Time per hop: %f\n", (double) delta_us/num_hops);
    return 0;
}
