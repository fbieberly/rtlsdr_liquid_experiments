/**
 * librh_hw.i
 * 
 * Internal API functions only.
 * 
 * Author(s): Abbas, Eric
 * 
 */

#ifndef RHRTLSDRLIB_I
#define RHRTLSDRLIB_I

/*
 * This head is used for intneral API functions only.
 */

/** Start SPI communications **/
int spiOpen(void);

/** End SPI communicaions **/
int spiClose(void);

/** Initialize the PLL of front end. **/
int pllInit(void);

/** Convenience Function for pllTune **/
int vcoSubsys2(int k);

/** Set the frequency of PLL **/
int pllTune(float freq);

/** Select the output through MUX **/
int selectOut(float freq);

/** Set the front end VGA Gain **/
int rh_set_vga_gain(float gain);

/** HW detect utility **/
int hw_detect(void);

#endif /* RHRTLSDRLIB_I */
