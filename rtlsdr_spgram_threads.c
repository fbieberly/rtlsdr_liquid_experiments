// This is the main demo that I wanted to put together.
// The RTLSDR is tuned to 100 MHz, with some assumption that a front-end 
// is doing the actual tuning to the desired frequency.
// The RTLSDR just collects samples contiguously. The front-end should hop every
// 'hop_time' seconds, then the samples in each hop are processed (with guard times)
// A lot goes on inside the simple calls to liquid-dsp's spgramcf object.
// There is good documentation on it here: http://liquidsdr.org/doc/spgram/
// 
// TODO: 
// All the data is collected before processing starts. Might as well do those simutaneously
// There is rolloff on the sides of the RTLSDRs spectrograms, those should be removed
// If there was really a front-end there probably needs to be some triggering of it so 
//   it knows when to start hopping.
// It would be fun to modify this so that the RTLSDR does the hopping, just to see it.
// The hop_time should probably be ~10x the nFFT, which should be like Fs/1e3 or 1e4?
// 
// gcc ./rtlsdr_spgram_threads.c -o ./rtl_tspgramcf.exe -lliquid -lm -lpthread -lrtlsdr -O2

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <sys/time.h>
#include <complex.h>

#include <liquid/liquid.h>
#include "rtl-sdr.h"

#define OUTPUT_FILENAME "plot_rtl_spgram_thread.py"
#define NUM_THREADS 4
static rtlsdr_dev_t *dev = NULL;

int n_threads;
unsigned int nfft;
float complex *buffer;
float *out_buffer;

int sample_rate = 2e6;
float guard_time = 50e-6;
float hop_time = 2560e-6;
int num_hops = 1000;
int frequency = 100000000;
int gain = 200;

// This function will run in a thread. It has access to the global buffer of 
// rtlsdr samples, the buffer of output samples, the number of threads, and nfft size
void *get_result(void *param)
{
    int guard_samples = sample_rate * guard_time;
    int hop_samples = sample_rate * hop_time;
    int thread_num = *(int *) param;

    // iterate across the buffer by strides that are determined by the number of threads
    for (int i = thread_num; i < num_hops; i += n_threads)
    {
        spgramcf q = spgramcf_create_default(nfft);
        for (int j = 0; j < hop_samples - guard_samples; ++j)
        {
            spgramcf_push(q, buffer[i*hop_samples + j]);
        }

        spgramcf_get_psd(q, out_buffer + (i * nfft));
        // this is probably inefficient, I bet I could just clear the spgram objects buffers
        // without destroying and recreating each time.
        spgramcf_destroy(q);
    }
}

int main() {
    unsigned int num_samples = (int) sample_rate * hop_time * num_hops + 4096;  // number of samples
    
    // spectral periodogram options
    n_threads = NUM_THREADS;
    nfft = 256;  // spectral periodogram FFT size
    buffer = malloc(num_samples * sizeof(float complex));
    out_buffer = malloc(num_hops * nfft * sizeof(float));

    // ###########################################################################
    // ###########################################################################
    // ###########################################################################
    // ###########################################################################
    // RTLSDR code
    // pretty much just copied from an example
    
    uint8_t *rtl_buffer = malloc(2*num_samples * sizeof(uint8_t));

    ///
    // init radio
    ///
    int device_count = rtlsdr_get_device_count();
    if (!device_count)
    {
        fprintf(stderr, "No supported devices found.\n");
        exit(1);
    }

    fprintf(stderr, "Found %d device(s):\n", device_count);

    uint32_t dev_index = 0;
    fprintf(stderr, "Using device %d: %s\n", dev_index, rtlsdr_get_device_name(dev_index));

    int r = rtlsdr_open(&dev, dev_index);
    if (r < 0)
    {
        fprintf(stderr, "Failed to open rtlsdr device #%d.\n", dev_index);
        exit(1);
    }
    
    // Set the sample rate
    r = rtlsdr_set_sample_rate(dev, (uint32_t) sample_rate);
    if (r < 0) fprintf(stderr, "WARNING: Failed to set sample rate.\n");

    // Set the frequency
    r = rtlsdr_set_center_freq(dev, frequency);
    if (r < 0) fprintf(stderr, "WARNING: Failed to set center freq.\n");
    else fprintf(stderr, "Tuned to %f MHz.\n", frequency/1e6);

    // Set the gain
    // Enable manual gain
    r = rtlsdr_set_tuner_gain_mode(dev, 1);
    if (r < 0) fprintf(stderr, "WARNING: Failed to enable manual gain.\n");

    // Set the tuner gain 
    r = rtlsdr_set_tuner_gain(dev, gain);
    if (r < 0)
    {
        fprintf(stderr, "WARNING: Failed to set tuner gain.\n");
        fprintf(stderr, "Valid values for e4000 are: -10, 15, 40, 65, 90, 115, 140, 165, 190, 215, 240, 290, 340, 420, 430, 450, 470, 490\n");
        fprintf(stderr, "Valid values for r820t are: 9, 14, 27, 37, 77, 87, 125, 144, 157, 166, 197, 207, 229, 254, 280, 297,\n\t328, 338, 364, 372, 386, 402, 421, 434, 439, 445, 480, 496\n");
        fprintf(stderr, "Gain values are in tenths of dB, e.g. 115 means 11.5 dB.\n");
    }
    else fprintf(stderr, "Tuner gain set to %f dB.\n", gain/10.0);

    // Reset endpoint before we start reading from it (mandatory)
    r = rtlsdr_reset_buffer(dev);
    if (r < 0) fprintf(stderr, "WARNING: Failed to reset buffers.\n");


    uint32_t out_block_size = (int) (2*num_samples/4096 + 1);
    printf("Block size: %d\n", out_block_size);

    int n_read;
    printf("Num Samples: %d\n", (int) out_block_size*4096);
    struct timeval  tv1, tv2;
    gettimeofday(&tv1, NULL);

    r = rtlsdr_read_sync(dev, rtl_buffer, out_block_size*4096, &n_read);
    if (r < 0)
    {
        fprintf(stderr, "WARNING: sync read failed -- %d\n", r);
        return -1;
    }
    //  convert the samples to complex floats
    //  the slightly odd '127.4' is taken from some osmocom code I found
    // https://www.reddit.com/r/RTLSDR/comments/1xo5l3/help_me_make_sense_of_rtl2832u_raw_iq_data/
    for (int i = 0; i < num_samples; ++i)
    {
        float i_val = (((float) rtl_buffer[i*2]) - 127.4) * 1.0/128.0;
        float q_val = (((float) rtl_buffer[i*2 + 1]) - 127.4) * 1.0/128.0;
        buffer[i] = i_val + q_val*I;
    }
    gettimeofday(&tv2, NULL);

    printf ("Data collection time = %f seconds\n",
         (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
         (double) (tv2.tv_sec - tv1.tv_sec));

    // ###########################################################################
    // ###########################################################################
    // ###########################################################################
    // ###########################################################################
    // Processing data
    int *val = malloc(n_threads* sizeof(int));
    pthread_t *tid = malloc( n_threads * sizeof(pthread_t) );

    gettimeofday(&tv1, NULL);

    for(int i=0; i<n_threads; i++ ){
        // printf("Create thread %d.\n", i);
        val[i] = i;
        pthread_create( &tid[i], NULL, get_result, (void *) &val[i] );
    }

    for(int i=0; i<n_threads; i++ ){
        // printf("Join thread %d.\n", i);
        pthread_join( tid[i], NULL );
    }
    gettimeofday(&tv2, NULL);

    printf ("Processing time = %f seconds\n",
         (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
         (double) (tv2.tv_sec - tv1.tv_sec));

    //
    // export output file
    //
    FILE * fid = fopen(OUTPUT_FILENAME,"w");
    fprintf(fid,"#%% %s : auto-generated file\n", OUTPUT_FILENAME);
    fprintf(fid,"import numpy as np\n");
    fprintf(fid,"import matplotlib.pyplot as plt\n");
    fprintf(fid,"nfft = %u;\n", num_hops * nfft);
    fprintf(fid,"psd  = np.zeros(nfft)\n");

    for (int i=0; i<num_hops * nfft; i++)
    {
        fprintf(fid,"psd[%6u] = %12.4e\n", i, out_buffer[i]);
    }

    fprintf(fid,"plt.plot(psd, '-')\n");
    fprintf(fid,"plt.show()\n");

    fclose(fid);
    printf("results written to %s.\n", OUTPUT_FILENAME);

    printf("done.\n");
    return 0;
}

