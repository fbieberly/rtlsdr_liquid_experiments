// Just a demo to show how to use pthreads
// 
// gcc ./thread_test.c -o ./thread.exe -lm -lpthread

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>


void *get_result(void *param)  // param is a dummy pointer
{
    int val = *(int *) param;
    printf("PID %d: param %d...\n", getpid(), val);
}

int main()
{
    int num_samples = 1000;
    int array[num_samples];

    printf("Starting program.\n");
    int ntimes = 20;
    int i = 0;
    pthread_t *tid = malloc( ntimes * sizeof(pthread_t) );
    int *val = malloc(sizeof(int));
    printf("Mallocs.\n");

    *val = 8;

    for ( i=0; i < num_samples; i++ ){
        array[i] = i + 1;
    }

    int sample_sum = 0;
    for ( i=0; i < num_samples; i++ ){
        sample_sum += array[i];
    }
    printf("Sum of samples: %d.\n", sample_sum);

    for( i=0; i<ntimes; i++ ){
        printf("Create thread %d.\n", i);

        pthread_create( &tid[i], NULL, get_result, (void *) val );
    }

    // do some tasks unrelated to result

    for( i=0; i<ntimes; i++ ){
        printf("Join thread %d.\n", i);

        pthread_join( tid[i], NULL );
    }

}