// This is an example straight from the liquid DSP repo
// it shows how to use the spgram interface by generating some
// fake data then processing it.
//
// gcc ./spgramcf.c -o ./spgramcf.exe -lliquid -lm

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <liquid/liquid.h>

#define OUTPUT_FILENAME "disp_spgramcf.py"

int main() {
    // spectral periodogram options
    unsigned int nfft        =   32;  // spectral periodogram FFT size
    unsigned int num_samples =    2e6;  // number of samples
    float        noise_floor = -60.0f;  // noise floor [dB]

    unsigned int i;
    float complex *buffer = malloc(num_samples * sizeof(float complex));
    float *out_buffer = malloc(num_samples * sizeof(float));

    // derived values
    float nstd = powf(10.0f, noise_floor/20.0f);

    // create spectral periodogram
    spgramcf q = spgramcf_create_default(nfft);
    spgramcf_print(q);

    // generate signal (band-pass filter with Hilbert transform)
    iirfilt_rrrf filter = iirfilt_rrrf_create_prototype(
            LIQUID_IIRDES_BUTTER,
            LIQUID_IIRDES_BANDPASS,
            LIQUID_IIRDES_SOS,
            9, 0.17f, 0.20f, 0.1f, 60.0f);
    firhilbf ht = firhilbf_create(13,60.0f);

    // create samples
    for (i=0; i<num_samples; i++) {
        // filter input noise signal
        float v = 0;
        iirfilt_rrrf_execute(filter, randnf(), &v);

        // filter off negative image (gain of 2)
        float complex y = 0;
        firhilbf_r2c_execute(ht, v, &y);

        // scale and add noise
        buffer[i] = 0.5f*y + nstd * ( randnf() + _Complex_I*randnf() ) * M_SQRT1_2;
    }

    // push resulting sample through periodogram
    for (i = 0; i < num_samples; ++i)
    {
        spgramcf_push(q, buffer[i]);
    }

    int sample_rate = 1e6;
    float guard_time = 50e-6;
    float hop_time = 300e-6;
    int num_hops = 1;

    int guard_samples = sample_rate * guard_time;
    int hop_samples = sample_rate * hop_time;
    printf("Guard samples: %d\n", guard_samples);
    // printf("Hop samples: %d\n", process_samples);



    // compute power spectral density output
    float psd[nfft];
    spgramcf_get_psd(q, psd);

    // destroy objects
    iirfilt_rrrf_destroy(filter);
    firhilbf_destroy(ht);
    spgramcf_destroy(q);

    //
    // export output file
    //
    FILE * fid = fopen(OUTPUT_FILENAME,"w");
    fprintf(fid,"#%% %s : auto-generated file\n", OUTPUT_FILENAME);
    fprintf(fid,"import numpy as np\n");
    fprintf(fid,"import matplotlib.pyplot as plt\n");
    fprintf(fid,"nfft = %u;\n", nfft);
    fprintf(fid,"psd  = np.zeros(nfft)\n");
    fprintf(fid,"noise_floor = %12.6f\n", noise_floor);

    for (i=0; i<nfft; i++)
        fprintf(fid,"psd[%6u] = %12.4e\n", i, psd[i]);

    fprintf(fid,"plt.plot(psd, '-')\n");
    fprintf(fid,"plt.show()\n");

    fclose(fid);
    printf("results written to %s.\n", OUTPUT_FILENAME);

    printf("done.\n");
    return 0;
}

