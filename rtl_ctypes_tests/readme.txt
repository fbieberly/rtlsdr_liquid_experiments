Dependencies:
python
numpy
scipy
matplotlib
rtlsdr library from: https://github.com/keenerd/rtl-sdr.git

you have to compile the c code into a shared object to be accessed by ctypes before running the python:
gcc ./rtlsdr_ctypes_lib.c -fPIC -shared -o ./rtlsdr_ctypes_lib.so -lrtlsdr

Results:
Initially, I've seen that I can scan 1 GHz in 12 seconds with this code. I do this by sampling at 2.56 MSps, collecting batches of samples which are all placed in a buffer as I hop every 2 MHz through the frequencies of interest (dropping the 0.56 MHz off the sides). The python code calls into some C code that interfaces with the RTLSDR and performs the scan. The C code returns a pointer to the python that is conveniently a numpy array. Then I use SciPy to perform the welch algorithm. I didn't bother to reproduce the exact sigpro that the radiohound software does, but the scipy welch does a lot of that stuff (overlap, windows, detrend, fft, mag squared).. except for the zero padding, which could be done in the C code?
Later I took out a line of code that clears the buffers of the rtlsdr and now can scan 1 GHz in ~5 seconds. I think clearing the buffers isn't the right way to go, it's faster to just skip the first XX samples (I don't know how big the buffer is) to be sure that the old data is gone. 

Caveats:
I've been writing this code on my laptop, so this performance might not reflect how a RPi works.. but it should be similar.
I haven't bothered to manage my memory use for the smaller memory of the RPi.. For a very big scan you could probably use up all of the memory on the RPi, but it should be easy to work around that. The RTLSDR produces data slow enough you could write it all to disk before processing it.
I haven't bothered to multi-thread anything. It really, at this point, doesn't matter. Collecting the data is by far the most time consuming part of the code.
Currently, I've been lazy and I re-initialize the RTLSDR every time I request a scan. That incurs a 300 millisecond penalty on every scan. It should be easy to make a couple C functions for opening and closing the dongle so that it's ready for data collection immediately.