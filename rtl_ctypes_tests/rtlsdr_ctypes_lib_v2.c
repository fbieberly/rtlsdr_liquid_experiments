// simple example of how to get samples from an RTLSDR
// I used the rtlsdr_read_sync method of getting samples
// there is an async version that you might be able to use better
// 
// gcc ./rtlsdr_ctypes_lib_v2.c -fPIC -shared -o ./rtlsdr_ctypes_lib.so -lrtlsdr

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "rtl-sdr.h"

#define SAMP_RATE 2400000
#define BUFFER_SIZE 1024*100
static rtlsdr_dev_t *dev = NULL;

int get_samples(int start_freq, int stop_freq, int rbw, int samps_per_bin,
                uint8_t *out_buffer)
{
    fprintf(stderr, "Getting samples from %d to %d.\n", start_freq, stop_freq);

    start_freq *= 1000000; // convert to MHz
    stop_freq *= 1000000;  // convert to MHz
    int samps_per_hop = SAMP_RATE / rbw * samps_per_bin * 2;
    fprintf(stderr, "Getting %d samples per hop.\n", samps_per_hop);
    uint8_t *buffer = malloc(BUFFER_SIZE * sizeof(uint8_t));

    ///
    // init radio
    ///
    // rtlsdr_dev_t *dev = (rtlsdr_dev_t *) dev_ptr;
    
    // Set the sample rate
    int r = rtlsdr_set_sample_rate(dev, SAMP_RATE);
    if (r < 0) fprintf(stderr, "WARNING: Failed to set sample rate.\n");

    // Set the center frequency
    r = rtlsdr_set_center_freq(dev, start_freq);
    if (r < 0) fprintf(stderr, "WARNING: Failed to set center freq.\n");
    else fprintf(stderr, "Tuned to %f MHz.\n", start_freq/1e6);

    // Set the gain
    int gain = 100;
    // Enable manual gain
    r = rtlsdr_set_tuner_gain_mode(dev, 1);
    if (r < 0) fprintf(stderr, "WARNING: Failed to enable manual gain.\n");

    // Set the tuner gain 
    r = rtlsdr_set_tuner_gain(dev, gain);
    if (r < 0)
    {
        fprintf(stderr, "WARNING: Failed to set tuner gain.\n");
        fprintf(stderr, "Valid values for e4000 are: -10, 15, 40, 65, 90, 115, 140, 165, 190, 215, 240, 290, 340, 420, 430, 450, 470, 490\n");
        fprintf(stderr, "Valid values for r820t are: 9, 14, 27, 37, 77, 87, 125, 144, 157, 166, 197, 207, 229, 254, 280, 297,\n\t328, 338, 364, 372, 386, 402, 421, 434, 439, 445, 480, 496\n");
        fprintf(stderr, "Gain values are in tenths of dB, e.g. 115 means 11.5 dB.\n");
    }
    else fprintf(stderr, "Tuner gain set to %f dB.\n", gain/10.0);

    // Reset endpoint before we start reading from it (mandatory)
    r = rtlsdr_reset_buffer(dev);
    if (r < 0) fprintf(stderr, "WARNING: Failed to reset buffers.\n");

    uint32_t out_block_size = samps_per_hop;
    int n_read;
    r = rtlsdr_read_sync(dev, buffer, out_block_size, &n_read);
    if (r < 0)
    {
        fprintf(stderr, "WARNING: sync read failed -- %d\n", r);
        return -1;
    }
    fprintf(stderr, "Samples read: %d\n", n_read);

    for (int i = 0; i < samps_per_hop; ++i)
    {   
        out_buffer[i] = buffer[i];
    }
    // r = rtlsdr_close(dev);
    // if (r < 0)
    // {
    //     fprintf(stderr, "Failed to close rtlsdr device.\n");
    //     exit(1);
    // }

    return 0;
}

int get_scan_samples(int start_freq, int num_hops, int rbw, int samps_per_bin,
                     int gain, uint8_t *out_buffer)
{
    fprintf(stderr, "Getting samples from %d, for %d hops.\n", start_freq, num_hops);

    start_freq *= 1000000; // convert to MHz
    int samps_per_hop = SAMP_RATE / rbw * samps_per_bin * 2;
    fprintf(stderr, "Getting %d samples per hop.\n", samps_per_hop);
    uint8_t *buffer = malloc(BUFFER_SIZE * sizeof(uint8_t));


    ///
    // init radio
    ///
    // rtlsdr_dev_t *dev = (rtlsdr_dev_t *) dev_ptr;
    if (dev == NULL) {
        fprintf(stderr, "ERROR: Device not opened.\n");
        return 1;
    }

    // Set the sample rate
    int r = rtlsdr_set_sample_rate(dev, SAMP_RATE);
    if (r < 0) fprintf(stderr, "WARNING: Failed to set sample rate.\n");

    // Set the gain
    // int gain = 100;
    // Enable manual gain
    r = rtlsdr_set_tuner_gain_mode(dev, 1);
    if (r < 0) fprintf(stderr, "WARNING: Failed to enable manual gain.\n");

    // Set the tuner gain 
    r = rtlsdr_set_tuner_gain(dev, gain);
    if (r < 0)
    {
        fprintf(stderr, "WARNING: Failed to set tuner gain.\n");
        fprintf(stderr, "Valid values for e4000 are: -10, 15, 40, 65, 90, 115, 140, 165, 190, 215, 240, 290, 340, 420, 430, 450, 470, 490\n");
        fprintf(stderr, "Valid values for r820t are: 9, 14, 27, 37, 77, 87, 125, 144, 157, 166, 197, 207, 229, 254, 280, 297,\n\t328, 338, 364, 372, 386, 402, 421, 434, 439, 445, 480, 496\n");
        fprintf(stderr, "Gain values are in tenths of dB, e.g. 115 means 11.5 dB.\n");
    }
    else fprintf(stderr, "Tuner gain set to %f dB.\n", gain/10.0);
    
    int out_buffer_idx = 0;
    int target_frequency = start_freq;
    // Reset endpoint before we start reading from it (mandatory)
    r = rtlsdr_reset_buffer(dev);
    if (r < 0) fprintf(stderr, "WARNING: Failed to reset buffers.\n");
    for (int i = 0; i < num_hops; ++i)
    {
        // Set the center frequency
        r = rtlsdr_set_center_freq(dev, target_frequency);
        if (r < 0) fprintf(stderr, "WARNING: Failed to set center freq.\n");
        // else fprintf(stderr, "Tuned to %f MHz.\n", target_frequency/1e6);


        uint32_t out_block_size = 0;
        out_block_size = 512*10;

        int n_read;
        r = rtlsdr_read_sync(dev, buffer, out_block_size, &n_read);
        if (r < 0)
        {
            fprintf(stderr, "WARNING: sync read failed -- %d\n", r);
            return -1;
        }
        // fprintf(stderr, "Samples read: %d\n", n_read);

        for (int i = 0; i < samps_per_hop; ++i)
        {   
            out_buffer[out_buffer_idx] = buffer[i];
            out_buffer_idx++;
        }

        // before next loop, update frequency
        target_frequency += 2000000;
    }
    // r = rtlsdr_close(dev);
    // if (r < 0)
    // {
    //     fprintf(stderr, "Failed to close rtlsdr device.\n");
    //     exit(1);
    // }

    return 0;
}

int open_rtlsdr(uint32_t dev_index)
{
    ///
    // init radio
    ///
    int device_count = rtlsdr_get_device_count();
    if (!device_count)
    {
        fprintf(stderr, "No supported devices found.\n");
        exit(1);
    }

    fprintf(stderr, "Found %d device(s):\n", device_count);

    // uint32_t dev_index = 0;
    fprintf(stderr, "Using device %d: %s\n", dev_index, rtlsdr_get_device_name(dev_index));

    int r = rtlsdr_open(&dev, dev_index);
    if (r < 0)
    {
        fprintf(stderr, "Failed to open rtlsdr device #%d.\n", dev_index);
        exit(1);
    }
    return 0;
}

int close_rtlsdr()
{
    ///
    // close radio
    ///
    int r = rtlsdr_close(dev);
    if (r < 0)
    {
        fprintf(stderr, "Failed to close rtlsdr device.\n");
        exit(1);
    }
    dev = NULL;

    return 0;
}