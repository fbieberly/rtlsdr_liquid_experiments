// simple example of how to get samples from an RTLSDR
// I used the rtlsdr_read_sync method of getting samples
// there is an async version that you might be able to use better
// 
// gcc ./rtl_time_hop.c -o ./rtl_time_hop.exe -lrtlsdr

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "rtl-sdr.h"

#define BUFFER_SIZE 1000000
static rtlsdr_dev_t *dev = NULL;

struct timespec start, end;

int main(int argc, char **argv)
{
    uint8_t *buffer = malloc(BUFFER_SIZE * sizeof(uint8_t));

    ///
    // init radio
    ///
    int device_count = rtlsdr_get_device_count();
    if (!device_count)
    {
        fprintf(stderr, "No supported devices found.\n");
        exit(1);
    }

    fprintf(stderr, "Found %d device(s):\n", device_count);

    uint32_t dev_index = 0;
    fprintf(stderr, "Using device %d: %s\n", dev_index, rtlsdr_get_device_name(dev_index));

    int r = rtlsdr_open(&dev, dev_index);
    if (r < 0)
    {
        fprintf(stderr, "Failed to open rtlsdr device #%d.\n", dev_index);
        exit(1);
    }
    
    int num_hops = 200;
    int frequency = 100000000;
    clock_gettime(NULL, &start);
    for (int i = 0; i < num_hops; ++i)
    {
        r = rtlsdr_set_center_freq(dev, frequency);
        if (r < 0) fprintf(stderr, "WARNING: Failed to set center freq.\n");

        frequency += 1000000;
    }
    clock_gettime(NULL, &end);

    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;

    fprintf(stderr, "Time per hop: %f\n", (double) delta_us/num_hops);
    return 0;
}